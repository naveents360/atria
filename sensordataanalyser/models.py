from django.db import models

# Create your models here.

class sensordata(models.Model):
    reading = models.FloatField(null=True, blank=True, default=None)
    sensorType = models.CharField(max_length=255,default='null')
    timestamp = models.DateTimeField(auto_now_add=True)

class addsensortype(models.Model):
    sensortype = models.CharField(max_length=255,default='null')
