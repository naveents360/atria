from django.apps import AppConfig


class SensordataanalyserConfig(AppConfig):
    name = 'sensordataanalyser'
