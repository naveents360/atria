# Generated by Django 2.2.4 on 2021-02-07 09:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sensordataanalyser', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='addsensortype',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sensortype', models.CharField(default='null', max_length=255)),
            ],
        ),
    ]
