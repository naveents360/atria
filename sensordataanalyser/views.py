from django.shortcuts import render

# Create your views here.

from django.conf import settings
from django.http import JsonResponse
from .models import sensordata, addsensortype
from .forms import sensordataForm, addsensortypeForm
from .serializers import sensordataSerializer, addsensortypeSerializer
from .utils import getmean, gettotal, decryptdata, encryptdata, validatedate, validatesensortype, validatetime
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework import exceptions
import logging
import json
import datetime
from django.db.models import Q

logger = logging.getLogger('django')

dataset = {"300":300,"900":900,"1800":1800,"3600":3600,"10800":10800,"21600":21600,"43200":43200,"86400":86400}

@api_view(['POST'])
def addsensordata_view(request):
    try:
        sensordetails = json.loads(request.body)
    except Exception as e:
        return JsonResponse({'message':'Invalid data format'}, status=201, safe=False)
    reading = sensordetails.get('reading')
    if reading == "":
        return JsonResponse({'message':'reading value should not be empty'}, status=201, safe=False)
    sensorType = sensordetails.get('sensorType')
    if sensorType == "":
        return JsonResponse({'message':'sensor Type should not be empty'}, status=201, safe=False)
    reading = float(reading)
    if (sensorType.lower() not in validatesensortype()):
        return JsonResponse({'message':'Invalid sensor Type'}, status=201, safe=False)
    sensordata = {"reading":float(reading),"sensorType":sensorType.lower()}
    form = sensordataForm(sensordata)
    if form.is_valid():
        form.save(commit=True)
        return JsonResponse({'message':'Data added successfully'}, status=200, safe=False)
    return JsonResponse({'message':'Error while adding the data'}, status=201, safe=False)

@api_view(['GET'])
def getsensordata_view(request):
    allsensordata = sensordata.objects.all()
    if not allsensordata:
        return JsonResponse({'msg':settings.NO_DATA_ERROR_MSG}, status=201, safe=False)
    serializer = sensordataSerializer(allsensordata, many=True)
    total,minimum,maximum, mean = gettotal(serializer.data)
    jsondata = {"mean":float(mean),"min":float(minimum),"max":float(maximum),"total":float(total),"sensordata":serializer.data}
    return JsonResponse(jsondata, status=200, safe=False)

@api_view(['GET'])
def getsensordatabysensor_view(request,sensorname):
    sensordatabysensor = sensordata.objects.filter(sensorType = str(sensorname))
    if not sensordatabysensor:
        return JsonResponse({'msg':settings.NO_DATA_ERROR_MSG}, status=201, safe=False)
    serializer = sensordataSerializer(sensordatabysensor, many=True)
    total,minimum,maximum,mean = gettotal(serializer.data)
    jsondata = {"mean":float(mean),"min":float(minimum),"max":float(maximum),"total":float(total),"sensordata":serializer.data}
    return JsonResponse(jsondata, status=200, safe=False)

@api_view(['GET'])
def getsensordatabydate_view(request):
    year = request.query_params.get('year')
    month = request.query_params.get('month')
    date = request.query_params.get('date')
    temp = validatedate(year,month,date)
    if temp is not None:
        return JsonResponse({'message':temp}, status=201, safe=False)
    sensordatabydate = sensordata.objects.filter(timestamp__contains=datetime.date(int(year),int(month),int(date)))
    if not sensordatabydate:
        return JsonResponse({'msg':settings.NO_DATA_ERROR_MSG}, status=201, safe=False)
    serializer = sensordataSerializer(sensordatabydate, many=True)
    total,minimum,maximum, mean = gettotal(serializer.data)
    jsondata = {"mean":float(mean),"min":float(minimum),"max":float(maximum),"total":float(total),"sensordata":serializer.data}
    return JsonResponse(jsondata, status=200, safe=False)

@api_view(['POST'])
def addsensortype_view(request):
    data = json.loads(request.body)
    sensortype = data.get('sensortype')
    data = addsensortype.objects.filter(sensortype=str(sensortype))
    if data:
        msg = "sensor type "+sensortype+" is already exist"
        return JsonResponse({'message': msg}, status=201, safe=False)
    data = {"sensortype":sensortype.lower()}
    form = addsensortypeForm(data)
    if form.is_valid():
        form.save()
        return JsonResponse({'message':'sensor type added successfully'}, status=200, safe=False)

@api_view(['GET'])
def getsensortypedata_view(request):
    return JsonResponse({"data":validatesensortype()}, status=200, safe=False)

@api_view(['GET'])
def getdatabytime_view(request):
    sensortype = request.query_params.get('sensortype')
    seconds = request.query_params.get('seconds')
    sensordatabydate = sensordata.objects.filter(sensorType = str(sensortype))
    serializer = sensordataSerializer(sensordatabydate, many=True)
    err,total, minimum, maximum, mean, arr = validatetime(sensordatabydate,int(seconds))
    if total == None:
        arr = []
        jsondata = {"id":"-","reading":settings.NO_DATA_ERROR_MSG,"sensorType":"-"}
        arr.append(jsondata)
        jsondata = {"mean":"-","min":"-","max":"-","total":"-","sensordata":arr}
        return JsonResponse(jsondata, status=200, safe=False)
    jsondata = {"mean":float(mean),"min":float(minimum),"max":float(maximum),"total":float(total),"sensordata":arr}
    return JsonResponse(jsondata, status=200, safe=False)
