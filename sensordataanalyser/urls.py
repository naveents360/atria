from django.urls import path

from sensordataanalyser import views

urlpatterns = [
        path('addsensordata', views.addsensordata_view),
        path('addsensortype', views.addsensortype_view),
        path('getsensordata', views.getsensordata_view),
        path('getsensortype', views.getsensortypedata_view),
        path('getsensordatabysensor/<str:sensorname>/', views.getsensordatabysensor_view),
        path('getsensordatabydate', views.getsensordatabydate_view),
        path('getdatabytime', views.getdatabytime_view),
        ]
