from rest_framework import serializers

from .models import sensordata, addsensortype

class sensordataSerializer(serializers.ModelSerializer):
    class Meta:
        model = sensordata
        fields = '__all__'

class addsensortypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = addsensortype
        fields = '__all__'
