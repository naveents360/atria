from .models import sensordata, addsensortype
import base64
import datetime
from django.utils.timezone import utc

dateinfo = {"01":"January","02":"February","03":"March","04":"April","05":"May","06":"June","07":"July","08":"August","09":"September","10":"October","11":"november","12":"December"}

def getmean(data):
    count = 0.0
    total = 0.0
    for each in data:
        total += float(each['reading'])
        count += 1
    return total/count

def gettotal(data):
    total = 0.0
    minimum = 10000.0
    maximum = 0.0
    count = 0
    for each in data:
        total += float(each['reading'])
        count += 1
        if (float(each['reading']) < float(minimum)):
            minimum = float(each['reading'])
        if (float(each['reading']) > float(maximum)):
            maximum = float(each['reading'])
    return total, minimum, maximum, total/count

def decryptdata(msg):
    decodedmsg = base64.b64decode(msg)
    return decodedmsg.decode('ascii')

def encryptdata(msg):
    encryptmsg = msg.encode('ascii')
    encryptmsg = base64.b64encode(encryptmsg)
    return encryptmsg.decode('ascii')

def validatedate(year,month,date):
    t2 = datetime.datetime.now()
    try:
        t1 = datetime.datetime(int(year),int(month),int(date))
    except Exception as e:
        msg = "month "+dateinfo[month]+" doesnot have "+str(date)+" days"
        return msg
    t = str(t2 - t1)
    if t[0] == "-":
        msg = "Year should be less than "+str(t2.strftime("%d/%m/%Y"))
        return msg
    return None

def validatesensortype():
    sensortypedata = addsensortype.objects.all()
    arr = []
    for each in sensortypedata:
        arr.append(each.sensortype.lower())
    return arr

def validatetime(data,seconds):
    now = datetime.datetime.utcnow().replace(tzinfo=utc)
    arr = []
    jsonarr = []
    for each in data:
        timediff = now - each.timestamp
        if (timediff.seconds < int(seconds)):
            jsondata = {"id":each.id,"reading":each.reading,"sensorType":each.sensorType,"timestamp":each.timestamp}
            jsonarr.append(jsondata)
            arr.append(each)
    if len(arr) == 0:
        e = "No data available"
        return e,None,None,None,None,None
    total, minimum, maximum, mean = gettotalarr(arr)
    return None,total, minimum, maximum, mean, jsonarr

def gettotalarr(data):
    total = 0.0
    minimum = 10000.0
    maximum = 0.0
    count = 0 
    for each in data:
        total += float(each.reading)
        count += 1
        if (float(each.reading) < float(minimum)):
            minimum = float(each.reading)
        if (float(each.reading) > float(maximum)):
            maximum = float(each.reading)
    return total, minimum, maximum, total/count
