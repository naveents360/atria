from django import forms
from .models import sensordata, addsensortype

class sensordataForm(forms.ModelForm):
    class Meta:
        model = sensordata
        fields = '__all__'

class addsensortypeForm(forms.ModelForm):
    class Meta:
        model = addsensortype
        fields = '__all__'
